# iOS Assignment CS

This is a placeholder README file with the instructions for the assingment. We expect you to build your own README file.

## Instructions

You can find all the instrucrtions for the assingment on [Assingment Instructions](https://docs.google.com/document/d/1zCIIkybu5OkMOcsbuC106B92uqOb3L2PPo9DNFBjuWg/edit?usp=sharing).

## Delivering the code
* Fork this repo and select the access level as private **[Check how to do it here](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html)**
* Go to settings and add the user **m-cs-recruitment@backbase.com** with the read access **[Check how to do it here](https://confluence.atlassian.com/bitbucket/grant-repository-access-to-users-and-groups-221449716.html)**
* Send an e-mail to **m-cs-recruitment@backbase.com** with your info and repo once the development is done

Please remember to work wilth small commits, it help us to see how you improve your code :)

## Used third party Libraries
* Alamofire 
* MBProgressHUD - Used to show loading view when calls APIs
* SDWebImage - Used this library in order cache images

## Custom progress bar
* Created a sub class of CALayer and used UIBezierPath,UILabel to show the movie ratings. 

## Top Horizontal Scrollview
* Added stackView as a subview of Scrollview, then whenever subviews of stackview increases scrollView content size also will get increased




